let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur" ],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]},
		talk: function(){
			console.log("Pikachu! I choose you!")
		}

	}

console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);
console.log("Result of talk method: ");
trainer.talk();



let myPokemon = {
			name: 'Pikachu',
			level: 12,
			health: 24,
			attack: 12,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon);

let myPokemon1 = {
			name: 'Geodude',
			level: 8,
			health: 16,
			attack: 12,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon1);

let myPokemon2 = {
			name: 'MewTwo',
			level: 100,
			health: 200,
			attack: 100,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon2);


function updatedHealth(pokemon, health){

	if(pokemon === myPokemon){
		let myPokemon = {
			name: 'Pikachu',
			level: 12,
			health: health,
			attack: 12,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon);

	}
	else if(pokemon === myPokemon1){
		let myPokemon1 = {
			name: 'Geodude',
			level: 8,
			health: health,
			attack: 12,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon1);
	} else if(pokemon === myPokemon2){
		let myPokemon2 = {
			name: 'MewTwo',
			level: 100,
			health: health,
			attack: 100,
			tackle: function(){
				console.log('This Pokemon tackled targetPokemon');
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint: function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon2);
	}
			


			};

/*updatedHealth(myPokemon1,-16);*/


function Pokemon(name, level){

			// Properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;

			// Methods
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);
				console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
			};

			this.faint = function(){
				console.log(this.name + ' fainted. ');
			};

		}

		// Create new instances of the 'Pokemon' object each with their unique properties
			let pikachu = new Pokemon('Pikachu', 12);
			let Geodude = new Pokemon('Geodude', 8);
			let MewTwo = new Pokemon('Geodude', 100);

			Geodude.tackle(pikachu);
			updatedHealth(myPokemon,16);
			MewTwo.tackle(Geodude);
			Geodude.faint();
			updatedHealth(myPokemon1,-84);

